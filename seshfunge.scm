#!/usr/bin/csi -ss

(use extras)
(use dyn-vector)

(define bf-space (char->integer #\space))
(define bf-smc (char->integer #\;))
(define bf-nop (char->integer #\z))
(define bf-sm-on (char->integer #\"))
(define bf-reverse (char->integer #\r))
(define bf-trampoline (char->integer #\#))
(define bf-dbg-stack (char->integer #\S))
(define bf-dbg-print (char->integer #\P))
(define bf-stop (char->integer #\@))
(define bf-north (char->integer #\^))
(define bf-south (char->integer #\v))
(define bf-east (char->integer #\>))
(define bf-west (char->integer #\<))
(define bf-output-char (char->integer #\,))
(define bf-output-int (char->integer #\.))
(define bf-duplicate (char->integer #\:))
(define bf-east-west-if (char->integer #\_))
(define bf-north-south-if (char->integer #\|))
(define bf-push-0 (char->integer #\0))
(define bf-push-1 (char->integer #\1))
(define bf-push-2 (char->integer #\2))
(define bf-push-3 (char->integer #\3))
(define bf-push-4 (char->integer #\4))
(define bf-push-5 (char->integer #\5))
(define bf-push-6 (char->integer #\6))
(define bf-push-7 (char->integer #\7))
(define bf-push-8 (char->integer #\8))
(define bf-push-9 (char->integer #\9))
(define bf-push-a (char->integer #\a))
(define bf-push-b (char->integer #\b))
(define bf-push-c (char->integer #\c))
(define bf-push-d (char->integer #\d))
(define bf-push-e (char->integer #\e))
(define bf-push-f (char->integer #\f))
(define bf-add (char->integer #\+))
(define bf-subtract (char->integer #\-))
(define bf-multiply (char->integer #\*))
(define bf-divide (char->integer #\/))
(define bf-rem (char->integer #\%))
(define bf-not (char->integer #\!))
(define bf-greater (char->integer #\`))
(define bf-remove (char->integer #\$))
(define bf-swap (char->integer #\\))
(define bf-get (char->integer #\g))
(define bf-put (char->integer #\p))
(define bf-go-away (char->integer #\?))
(define bf-fetch-char (char->integer #\'))
(define bf-left (char->integer #\[))
(define bf-right (char->integer #\]))
(define bf-store-char (char->integer #\s))
(define bf-jump (char->integer #\j))
(define bf-absolute-delta (char->integer #\x))
(define bf-input-char (char->integer #\~))
(define bf-clear-stack (char->integer #\n))

(define (apply-instruction op)
        (lambda (fs ip delta stack sm stop) (let-values (((nstack vs) (bf-pop stack 2))) (values fs ip delta (bf-push nstack (apply op (reverse vs))) sm stop))))

(define (push-instruction num)
        (lambda (fs ip delta stack sm stop) (values fs ip delta (bf-push stack num) sm stop)))

(define (get-quadrant fs x y)
        ((if (>= y 0) car cdr) ((if (>= x 0) car cdr) fs)))

(define (quadrant-ref q x y)
        (let ((row (dynvector-ref q y)))
                (if (not row)
                        bf-space
                        (dynvector-ref row x))))

(define (funge-space-ref fs x y)
        (let ((q (get-quadrant fs x y))
              (nx (if (>= x 0) x (- (* x -1) 1)))
              (ny (if (>= y 0) y (- (* y -1) 1))))
                (quadrant-ref q nx ny)))

(define (quadrant-set! q x y v)
        (let ((row (dynvector-ref q y)))
                (if (not row)
                        (begin
                                (dynvector-set! q y (make-dynvector 1 bf-space))
                                (quadrant-set! q x y v))
                        (dynvector-set! row x v))))

(define (funge-space-set! fs x y v)
        (let ((q (get-quadrant fs x y))
              (nx (if (>= x 0) x (- (- x) 1)))
              (ny (if (>= y 0) y (- (- y) 1))))
                (quadrant-set! q nx ny v)))

; the below is an association list of befunge instructions
(define bf-instructions
	(list
		(list bf-space
			(lambda (fs ip delta stack sm stop) (display "In a Befunge 98 program/interpreter, space should never be executed, ever. If you see this, the interpreter is wrong!\n" (current-error-port)) (values fs ip delta stack sm stop))
			"is skipped over by the interpreter")
		(list bf-smc
                        (lambda (fs ip delta stack sm stop) (display "In a Befunge 98 program/interpreter, semicolon should never be executed, ever. If you see this, the interpreter is wrong!\n" (current-error-port)) (values fs ip delta stack sm stop))
                        "causes the ip to jump over all subsequent instructions until the next ;")
		(list bf-nop
			(lambda (fs ip delta stack sm stop) (values fs ip delta stack sm stop))
			"user is eaten by a grue")
		(list bf-sm-on
			(lambda (fs ip delta stack sm stop) (values fs ip delta stack #t stop))
			"turns string mode on, until another \" is reached, this pushes the character value of each reached cell to the stack")
		(list bf-reverse
			(lambda (fs ip delta stack sm stop) (values fs ip (cons (* (car delta) -1) (* (cdr delta) -1)) stack sm stop))
			"multiplies delta by -1, thus reversing the direction")
		(list bf-trampoline
			(lambda (fs ip delta stack sm stop) (values fs (cons (+ (car ip) (car delta)) (+ (cdr ip) (cdr delta))) delta stack sm stop))
			"jumps over the next cell")
		(list bf-dbg-stack
			(lambda (fs ip delta stack sm stop) (display stack) (values fs ip delta stack sm stop))
			"prints the stack, is non-standard")
		(list bf-dbg-print
                        (lambda (fs ip delta stack sm stop) (let-values (((nstack v) (bf-pop stack 1))) (format #t "~a ~s" (car v) (integer->char (car v))) (values fs ip delta nstack sm stop)))
                        "prints the first item on the stack, is non-standard")
		(list bf-stop
			(lambda (fs ip delta stack sm stop) (values fs ip delta stack sm #t))
			"gg")
		(list bf-north
                        (lambda (fs ip delta stack sm stop) (values fs ip '(0 . -1) stack sm stop))
                        "delta now points north")
                (list bf-south
                        (lambda (fs ip delta stack sm stop) (values fs ip '(0 . 1) stack sm stop))
                        "delta now points south")
                (list bf-east
                        (lambda (fs ip delta stack sm stop) (values fs ip '(1 . 0) stack sm stop))
                        "delta now points east")
                (list bf-west
                        (lambda (fs ip delta stack sm stop) (values fs ip '(-1 . 0) stack sm stop))
                        "delta now points west")
		(list bf-output-char
			(lambda (fs ip delta stack sm stop) (let-values (((nstack v) (bf-pop stack 1))) (if (equal? (car v) 10) (newline) (display (integer->char (car v)))) (values fs ip delta nstack sm stop)))
			"prints the first item on the stack as a character")
                (list bf-output-int
                        (lambda (fs ip delta stack sm stop) (let-values (((nstack v) (bf-pop stack 1))) (display (car v)) (values fs ip delta nstack sm stop)))
                        "prints the first item on the stack as an integer")
		(list bf-duplicate
                        (lambda (fs ip delta stack sm stop) (let ((nstack (bf-push stack (bf-peek stack)))) (values fs ip delta nstack sm stop)))
                        "duplicates the top item on the stack")
		(list bf-east-west-if
                        (lambda (fs ip delta stack sm stop) (let*-values (((nstack v) (bf-pop stack 1)) ((ndelta) (if (equal? (car v) 0) '(1 . 0) '(-1 . 0)))) (values fs ip ndelta nstack sm stop)))
                        "pops a value off the stack; if it is zero it acts like >, and if non-zero it acts like <")
                (list bf-north-south-if
                        (lambda (fs ip delta stack sm stop) (let*-values (((nstack v) (bf-pop stack 1)) ((ndelta) (if (equal? (car v) 0) '(0 . 1) '(0 . -1)))) (values fs ip ndelta nstack sm stop)))
                        "pops a value off the stack; if it is zero it acts like v, and if non-zero it acts like ^")
		(list bf-push-0
                        (push-instruction 0)
                        "pushes 0 to the stack")
		(list bf-push-1
			(push-instruction 1)
                        "pushes 1 to the stack")
		(list bf-push-2
                        (push-instruction 2)
                        "pushes 2 to the stack")
		(list bf-push-3
                        (push-instruction 3)
                        "pushes 3 to the stack")
		(list bf-push-4
                        (push-instruction 4)
                        "pushes 4 to the stack")
		(list bf-push-5
                        (push-instruction 5)
                        "pushes 5 to the stack")
		(list bf-push-6
                        (push-instruction 6)
                        "pushes 6 to the stack")
		(list bf-push-7
                        (push-instruction 7)
                        "pushes 7 to the stack")
		(list bf-push-8
                        (push-instruction 8)
                        "pushes 8 to the stack")
		(list bf-push-9
                        (push-instruction 9)
                        "pushes 9 to the stack")
		(list bf-push-a
                        (push-instruction 10)
                        "pushes 10 to the stack")
		(list bf-push-b
                        (push-instruction 11)
                        "pushes 11 to the stack")
		(list bf-push-c
                        (push-instruction 12)
                        "pushes 12 to the stack")
		(list bf-push-d
                        (push-instruction 13)
                        "pushes 13 to the stack")
		(list bf-push-e
                        (push-instruction 14)
                        "pushes 14 to the stack")
		(list bf-push-f
                        (push-instruction 15)
                        "pushes 15 to the stack")
		(list bf-add
			(apply-instruction +)
			"pops two values, pushes their sum")
		(list bf-subtract
                        (apply-instruction -)
                        "pops two values, pushes their difference")
		(list bf-multiply
                        (apply-instruction *)
                        "pops two values, pushes their product")
                (list bf-divide
                        (apply-instruction (lambda (a b) (if (equal? b 0) 0 (quotient a b))))
                        "pops two values, pushes their division")
		(list bf-rem
                        (apply-instruction (lambda (a b) (if (equal? b 0) 0 (remainder a b))))
                        "pops two values, pushes the remainder of their division")
		(list bf-greater
                        (apply-instruction (lambda (a b) (if (> a b) 1 0)))
                        "pops two cells off the stack, then pushes a 1 if second cell is greater than the first, otherwise pushes a 0")
		(list bf-not
			(lambda (fs ip delta stack sm stop) (let-values (((nstack v) (bf-pop stack 1))) (values fs ip delta (bf-push nstack (if (equal? (car v) 0) 1 0)) sm stop)))
			"pops a value off the stack and pushes a value which is the logical negation of it. If the value is 0, it pushes 1; if it is not 0, it pushes 0")
		(list bf-remove
			(lambda (fs ip delta stack sm stop) (let-values (((nstack v) (bf-pop stack 1))) (values fs ip delta nstack sm stop)))
			"pops a value off the stack")
		(list bf-swap
                        (lambda (fs ip delta stack sm stop) (let-values (((nstack vs) (bf-pop stack 2))) (values fs ip delta (bf-push (bf-push nstack (car vs)) (cadr vs)) sm stop)))
			"swaps the two topmost stack values")
		(list bf-get
			(lambda (fs ip delta stack sm stop) (let-values (((nstack vs) (bf-pop stack 2))) (values fs ip delta (bf-push nstack (funge-space-ref fs (cadr vs) (car vs))) sm stop)))
			"pops a vector (that is, it pops a y value, then an x value,) and pushes the value (character) in the funge-space cell at (x, y) onto the stack")
		(list bf-put
			(lambda (fs ip delta stack sm stop) (let-values (((nstack vs) (bf-pop stack 3))) (funge-space-set! fs (cadr vs) (car vs) (caddr vs)) (values fs ip delta nstack sm stop)))
			"pops a vector, then it pops a value, then it places that value in the funge-space cell at (x, y)")
		(list bf-fetch-char
			(lambda (fs ip delta stack sm stop) (let* ((nip (cons (+ (car ip) (car delta)) (+ (cdr ip) (cdr delta)))) (x (car nip)) (y (cdr nip)) (nstack (bf-push stack (funge-space-ref fs x y)))) (values fs nip delta nstack sm stop)))
			"pushes the funge character value of the next encountered cell (position + delta) onto the stack, then adds the delta to the position (like #), skipping over the character")
		(list bf-store-char
                        (lambda (fs ip delta stack sm stop) (let*-values (((nip) (cons (+ (car ip) (car delta)) (+ (cdr ip) (cdr delta)))) ((x) (car nip)) ((y) (cdr nip)) ((nstack v) (bf-pop stack 1))) (funge-space-set! fs x y (car v)) (values fs nip delta nstack sm stop)))
                        "this instead pops a value off the stack and writes it into (position + delta) (the spec is unclear on whether this should then skip over delta, however FBBI does)")
		(list bf-left
			(lambda (fs ip delta stack sm stop) (values fs ip (cons (cdr delta) (- (car delta))) stack sm stop))
			"rotate delta 90 degrees anticlockwise")
		(list bf-right
                        (lambda (fs ip delta stack sm stop) (values fs ip (cons (- (cdr delta)) (car delta)) stack sm stop))
                        "rotate delta 90 degrees clockwise")
		(list bf-jump
			(lambda (fs ip delta stack sm stop) (let*-values (((nstack v) (bf-pop stack 1)) ((nip) (cons (+ (car ip) (* (car v) (car delta))) (+ (cdr ip) (* (car v) (cdr delta)))))) (values fs nip delta nstack sm stop)))
                        "pops a value off the stack, and jumps over that many spaces")
		(list bf-absolute-delta
			 (lambda (fs ip delta stack sm stop) (let*-values (((nstack v) (bf-pop stack 2)) ((ndelta) (cons (cadr v) (car v)))) (values fs ip ndelta nstack sm stop)))
                        "pops a vector off the stack, and sets the ip delta to that vector")
		(list bf-input-char
                        (lambda (fs ip delta stack sm stop) (let ((nstack (bf-push stack (char->integer (read-char))))) (values fs ip delta nstack sm stop)))
                        "wait for the user to enter a character to the standard input")
		(list bf-clear-stack
                        (lambda (fs ip delta stack sm stop) (values fs ip delta (list) sm stop))
                        "clears the stack")
		(list bf-compare
                        (lambda (fs ip delta stack sm stop) (let*-values (((nstack vs) (bf-pop stack 2)) (call-with-values (lambda () (values fs ip delta nstack sm stop)) (get-instruction (cond ((> (cadr vs) (car vs)) bf-right bf-left) ((< (cadr vs) (car vs)) bf-left) (#t bf-nop)))))))
			"pops a value b off the stack, then pops a value a, then compares them; if the a is smaller, w acts like '[', if the a is greater, w acts like ']'. If a and b are equal, w does nothing")
		(list bf-go-away
			(lambda (fs ip delta stack sm stop) (values fs ip (case (random 4) ((0) '(0 . -1)) ((1) '(0 . 1)) ((2) '(-1 . 0)) ((3) '(1 . 0))) stack sm stop))
			"causes the ip to travel in a random cardinal direction")))

(define (get-instruction code)
	(or (assoc code bf-instructions) (assoc bf-reverse bf-instructions)))

; the format of the befunge funge-space (a 2D grid, extendible in both directions)
; is as such:
; four lists, one for each combination of negative-positive coordinates (e.g ++ +- -+ --)
; the general read function will take an x, a y and the space
; as with standard scheme, functions ending with ! modify things in place

(define (string->numdvec str)
                (list->dynvector (map char->integer (string->list str)) bf-space))

(define (read-quadrant port)
	(list->dynvector (read-quadrant-list port) #f))

(define (read-quadrant-list port)
	(let ((line (read-line port)))
        	(if (eof-object? line)
			(list #f)
			(cons (string->numdvec line) (read-quadrant-list port)))))

(define (funge-space-from-port port)
; returns a new space from the string data from port
; (only fills out the ++ quadrant, 1st char, 1st line is (1, 1), however the other quadrants are returned as empty)
	; ((++ . +-) . (-+ . --))
	(cons (cons (read-quadrant port) (make-dynvector 1 #f)) (cons (make-dynvector 1 #f) (make-dynvector 1 #f))))

(define (max-x fs y)
	(let* ((q (get-quadrant fs 1 y))
	       (row (dynvector-ref q y)))
		(if row
			(- (dynvector-length row) 1)
			0)))

(define (min-x fs y)
        (let* ((q (get-quadrant fs -1 y))
               (row (dynvector-ref q y)))
                (if row
                        (* (dynvector-length row) -1)
                	0)))

(define (max-y fs x)
	(let* ((q (get-quadrant fs x 1))
	       (l (- (dynvector-length q) 1)))
		(if (or (> l 0) (dynvector-ref q 0))
			l
			0)))
			
(define (min-y fs x)
        (let* ((q (get-quadrant fs x -1))
               (l (* (dynvector-length q) -1)))
                (if (or (< l -1) (dynvector-ref q 0))
                        l
                        0)))

; moves to the next cell in direction delta (with wraparound)
(define (seek-1 fs ip delta)
	(let* ((x (car ip))
	       (y (cdr ip))
	       (dx (car delta))
	       (dy (cdr delta))
	       (nx (+ x dx))
	       (nnx (if (equal? dx 0) nx (if (> nx (max-x fs y)) (min-x fs y) (if (< nx (min-x fs y)) (max-x fs y) nx))))
	       (ny (+ y dy))
	       (nny (if (equal? dy 0) ny (if (> ny (max-y fs nnx)) (min-y fs nnx) (if (< ny (min-y fs nnx)) (max-y fs nnx) ny)))))
		; jump jump jump!
		(cons nnx nny)))

; returns the location of the first semicolon starting from ip in direction delta
(define (seek-smc fs ip delta)
	(let* ((v (funge-space-ref fs (car ip) (cdr ip))))
		(if (equal? bf-smc v)
			ip
			(seek-smc fs (seek-1 fs ip delta) delta))))

; returns the location of the first non-space cell starting from ip in direction delta (interprets semicolons)
(define (seek-first fs ip delta)
	(let ((v (funge-space-ref fs (car ip) (cdr ip))))
		(cond
			((equal? v bf-space)
				(seek-first fs (seek-1 fs ip delta) delta))
			((equal? v bf-smc)
				(seek-next fs (seek-smc fs ip delta) delta))
			(#t
				ip))))

; returns the location of the *next* non-space cell starting from ip in direction delta (interprets semicolons)
(define (seek-next fs ip delta)
	(seek-first fs (seek-1 fs ip delta) delta))

; returns a list of n values popped from stack, also the new stack with those values popped
(define (bf-pop stack n)
	(if (> n 0)
		(let-values (((nstack vs) (bf-pop (if (equal? stack '()) stack (cdr stack)) (- n 1)))
		             ((v) (if (equal? stack '()) 0 (car stack))))
			(values
				nstack
				(cons v vs)))
		(values
			stack
			(list))))

(define (bf-push stack v)
	(cons v stack))

(define (bf-peek stack)
	(let-values (((s v) (bf-pop stack 1))) (car v)))

; returns the interpreter state after 1 step (note: fs is destructively modified)
(define (bf-step fs ip delta stack sm stop)
	(if (not sm)
		(let* ((ip (seek-first fs ip delta))
		       (instr (get-instruction (funge-space-ref fs (car ip) (cdr ip)))))
			(let-values (((nfs nip ndelta nstack nsm nstop) ((cadr instr) fs ip delta stack sm stop)))
				(if nsm
					(values nfs (seek-1 fs nip ndelta) ndelta nstack nsm nstop)
					(values nfs (seek-next fs nip ndelta) ndelta nstack nsm nstop))))
		(let* ((v (funge-space-ref fs (car ip) (cdr ip)))
		       (nip (seek-1 fs ip delta))
		       (nv (funge-space-ref fs (car nip) (cdr nip))))
			(cond
				((equal? v bf-sm-on)
					(values fs (seek-next fs ip delta) delta stack #f stop))
				((equal? v bf-space)
					(if (equal? nv bf-space)
						(values fs (seek-1 fs ip delta) delta stack sm stop)
						(values fs (seek-1 fs ip delta) delta (bf-push stack v) sm stop)))
				(#t
					(values fs (seek-1 fs ip delta) delta (bf-push stack v) sm stop))))))

(define (bf-exec fs ip delta stack sm stop)
	;(flush-output)
	;(display delta)
	(if stop
		(values fs ip delta stack sm stop)
		(call-with-values (lambda () (bf-step fs ip delta stack sm stop)) bf-exec))) 

(define (main args)
	(let ((fname (car args)))
		(if (file-exists? fname)
			(let ((fs (call-with-input-file fname funge-space-from-port))
			      (ip '(0 . 0))
			      (delta '(1 . 0))
			      (stack (list))
			      (sm #f)
			      (stop #f))
				(bf-exec fs ip delta stack sm stop)
				0)
			(begin
				(display "Error: File not found '" (current-error-port))
				(display fname (current-error-port))
				(display "'" (current-error-port))
				(newline (current-error-port))
				2))))

;(main (command-line-arguments)) ; uncomment this for compilation (as opposed to interpretation)

