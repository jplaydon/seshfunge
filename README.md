This is my attempt at creating a fully standards compliant Befunge-98 interpreter.

The Scheme used is Chicken Scheme, and the program uses at least one Chicken-specific feature: the 'dyn-vector' library.

I started this project in an attempt to learn how to:

- write Scheme (a fantastic language)
- use Git
- write Befunge (useful!)
- touch-type in Dvorak (almost all of the code was touch-typed *very slowly*)

Features
========

Now
---

- All of Befunge-93 (except `&` ("Input Integer"))
- Bits of Befunge-98

Future
------

- All of Befunge-93 & 98
- Specify the size of the Funge-space (eg 80x25 for full Befunge-93 compatibility)
- Concurrent Befunge (`t` instuction)
- Some fingerprints, including:
	- 'ROMA' Roman Numerals
	- 'HRTI' High-Resolution Timer Interface
	- 'TOYS' (maybe)
	- 'TURT' (maybe)
	- And possibly a new fingerprint to allow the befunge program to become an IRC client (for bottery)
- Another possibility is an Ncurses-based live Funge-space viewer with:
	- Step-through (One execution step per press of enter)
	- Slow-execution
	- Edit the program during execution
- An option to ignore the first line of the befunge file (for a #! line)
- A makefile, for automatic compilation and installation
- Have pre-built executables for multiple platforms (Chicken does not require its own presence if there is a compiled executable)
- Unifunge mode

Install
=======

- Chicken Scheme (http://www.call-cc.org/)
- 'dyn-vector' Chicken egg
	- Run `chicken-install dyn-vector` or equivalent (you must have permission to do so, so you might want to prefix that with `sudo` (Linux boxen) or run it as an Administrator (Windows machines) depending on how you install Chicken, the Chicken website should have all appropriate details)
- Compilation instrucions forthcoming, though as long as there is only one scheme file, `csc seshfunge.scm` might work

Run
===

- Currently seshfunge only takes one command-line argument, the name of a Befunge source file, all others are disregarded
- Chicken also comes with an interpreter, `csi seshfunge.scm hello.b98` may work to run the program too, though it will be significantly slower than with a compiled executable

